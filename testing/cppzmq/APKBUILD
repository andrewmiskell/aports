# Contributor: Andreas Laghamn <andreas.laghamn@gmail.com>
# Maintainer:
pkgname=cppzmq
pkgver=4.8.0
pkgrel=0
pkgdesc="High-level C++ binding for ZeroMQ"
options="!check" # CMake fails to find catch_discover_tests
url="https://github.com/zeromq/cppzmq"
arch="noarch"
license="MIT"
makedepends="cmake zeromq-dev catch2"
source="$pkgname-$pkgver.tar.gz::https://github.com/zeromq/cppzmq/archive/v$pkgver.tar.gz
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DCPPZMQ_BUILD_TESTS=OFF \
		$CMAKE_CROSSOPTS .
	make -C build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make -C build DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/lib
	mv "$pkgdir"/usr/share/cmake "$pkgdir"/usr/lib
}

sha512sums="
e2e099668870e2a8ec031b277a81cf89b01c2664393660d0a29355400e986fd4e92aacac13c4248c4eb4aab29825e19b7a3ba05086597040c780e9487b0949f2  cppzmq-4.8.0.tar.gz
"
